# EP1 - OO (UnB - Gama)

Este projeto consiste em um jogo de labirinto em C++ produzido por Gabriel Ziegler para a disciplina de Orientacao a objetos na Universidade de Brasilia.

O player é representado no jogo pelo caracter '@'.

O player deve evitar encostar nas armadilhas representadas pelos caracteres '#' que irao se movimentar aleatoriamente pelo mapa

Bonus podem ser pegos quando o player encostar em caracteres do tipo '&' para receber diferentes tipos de bonus.

Para vencer basta levar o player para o '8' com pontuacao positiva, porem o player perde uma vida a cada colisao com armadilhas e perde pontuacao a cada movimento. Caso as vidas cheguem a 0 é decretado o Game Over.

Ao fim do jogo se tiver passado com sucesso tera a chance de deixar seu nome na Lista dos Lendarios, reservada apenas para aqueles

que merecem estar la.

para movimentar o player utilize:

W ou ↑- para mover para cima

S ou ↓- para mover para baixo

A ou ← - para mover para esquerda

D ou → - para mover para direita

JOGUE COM O TERMINAL EM TELA CHEIA! :)
